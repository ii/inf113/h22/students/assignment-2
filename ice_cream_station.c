#include <pthread.h> // pthread
#include <stdio.h>   // prinftf
#include <stdlib.h>  // random
#include <unistd.h>  // sleep
// you might need more header files

void *worker_actions(void *employee_id);
void *customer_actions(void *personal_id);

#define NUM_CUSTOMERS 5
#define NUM_WORKERS 2

int main(int argc, char **argv) {

    int personal_ids[NUM_CUSTOMERS], employee_ids[NUM_WORKERS];

    pthread_t customers[NUM_CUSTOMERS], workers[NUM_WORKERS];

    // create treads
    for (int i = 0; i < NUM_WORKERS; i++) {
        employee_ids[i] = i;
        pthread_create(&workers[i], NULL, worker_actions, (void *)&employee_ids[i]);
    }
    for (int i = 0; i < NUM_CUSTOMERS; i++) {
        personal_ids[i] = i;
        pthread_create(&customers[i], NULL, customer_actions, (void *)&personal_ids[i]);
    }

    // join threads
    for (int i = 0; i < NUM_WORKERS; i++) {
        pthread_join(workers[i], NULL);
    }
    for (int i = 0; i < NUM_CUSTOMERS; i++) {
        pthread_join(customers[i], NULL);
    }

    return 0;
}

void *worker_actions(void *employee_id) {
    // get the id of this employee
    int id = *(int *)employee_id;

    while (1) {
    }
}

void *customer_actions(void *personal_id) {
    // get the id of this customer
    int id = *(int *)personal_id;

    while (1) {
    }
}